const Order = require("../models/Order");
const User = require("../models/User");
const ErrorController = require("./ErrorController");
const OrderController = {};

OrderController.getAllOrders = (request, response) => {
  return Order.find({})
    .then((db_orders) => {
      if (!db_orders.length)
        return response
          .status(200)
          .json({ message: "There are currently no orders" });

      return response.status(200).json({ data: db_orders });
    })
    .catch((error) => response.status(500).json({ error: error.message }));
};

OrderController.getUserOrders = (request, response) => {
  return Order.find({ username: request.params.username })
    .then(async (db_orders) => {
      if (
        !(await User.findById(request.params.username).then(
          (db_user) => db_user
        ))
      )
        throw new ErrorController.UserIdError();

      if (!db_orders.length)
        return response
          .status(200)
          .json({ message: "User currently has no purchased orders" });

      return response.status(200).json({ data: db_orders });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

OrderController.checkoutOrder = (request, response) => {
  return User.findById(request.params.username)
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();

      if (!db_user.cart.products.length) {
        return response
          .status(200)
          .json({ message: "User's cart is currently empty" });
      }

      let new_order = new Order({
        username: db_user._id,
        products: db_user.cart.products,
        totalAmount: db_user.cart.totalAmount,
      });

      return new_order.save().then((purchased_order) => {
        return response.status(201).json({
          message: "User's order purchased successfully",
          data: purchased_order,
        });
      });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

module.exports = OrderController;
