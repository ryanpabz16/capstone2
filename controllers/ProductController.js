const Product = require("../models/Product");
const ErrorController = require("./ErrorController");

const ProductController = {};

ProductController.getAllProducts = (_, response) => {
  return Product.find({})
    .then((db_products) => {
      if (!db_products.length) {
        return response
          .status(200)
          .json({ message: "There are currently no products" });
      }

      return response.status(200).json({ data: db_products });
    })
    .catch((error) =>
      response.status(500).json({ error: error.name, message: error.message })
    );
};

ProductController.getActiveProducts = (_, response) => {
  return Product.find({ isActive: true })
    .then((db_products) => {
      if (!db_products.length) {
        return response
          .status(200)
          .json({ message: "There are currently no active products" });
      }

      return response.status(200).json({ data: db_products });
    })
    .catch((error) =>
      response.status(500).json({ error: error.name, message: error.message })
    );
};

ProductController.getProduct = (request, response) => {
  return Product.findById(request.params.productId)
    .then((db_product) => {
      if (!db_product) throw new ErrorController.ProductIdError();

      return response.status(200).json({ data: db_product });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

ProductController.createProduct = (request, response) => {
  const {
    title,
    description,
    price,
    platforms,
    genre,
    metascore,
    releaseDate,
    publisher,
    developer,
    ageRating,
  } = request.body;

  return Product.findOne({ title: title })
    .then((db_product) => {
      if (!title || !description || !price)
        throw new ErrorController.ProductInfoError();
      if (db_product) throw new ErrorController.ProductTitleError();

      let new_product = new Product({
        title: title,
        description: description,
        price: price,
        platforms: platforms,
        genre: genre,
        metascore: metascore,
        releaseDate: releaseDate,
        publisher: publisher,
        developer: developer,
        ageRating: ageRating,
      });

      return new_product.save().then((created_product) => {
        return response.status(201).json({
          message: "New product created successfully",
          data: created_product,
        });
      });
    })
    .catch((error) => {
      if (error.name == "ValidationError" || error.name == "CastError") {
        error = new ErrorController.ProductTypeError();
      }

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

ProductController.updateProduct = async (request, response) => {
  let { title, description, price } = request.body;

  try {
    await Product.findOne({ title: title }).then((db_product) => {
      if (!title && !description && !price)
        throw new ErrorController.ProductUpdateError();
      if (db_product && db_product._id != request.params.productId) {
        throw new ErrorController.ProductTitleError();
      }
    });
  } catch (error) {
    let message;
    if (error.name == "CastError")
      message = new ErrorController.ProductTypeError().message;

    return response.status(error.status_code ?? 500).json({
      error: error.name,
      message: message ?? error.message,
    });
  }

  return Product.findById(request.params.productId)
    .then((db_product) => {
      if (!db_product) throw new ErrorController.ProductIdError();

      if (title) db_product.title = title;
      if (description) db_product.description = description;
      if (price) db_product.price = price;

      return db_product.save().then((updated_product) => {
        return response.status(200).json({
          message: "Product updated successfully",
          data: updated_product,
        });
      });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();
      if (error.name == "ValidationError")
        error = new ErrorController.ProductTypeError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

ProductController.archiveProduct = (request, response) => {
  return Product.findByIdAndUpdate(
    request.params.productId,
    { isActive: false },
    { new: true }
  )
    .then((db_product) => {
      return response.status(200).json({
        message: "Product has been archived",
        data: db_product,
      });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

ProductController.activateProduct = (request, response) => {
  return Product.findByIdAndUpdate(
    request.params.productId,
    { isActive: true },
    { new: true }
  )
    .then((db_product) => {
      return response.status(400).json({
        message: "Product has been set to active",
        data: db_product,
      });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

module.exports = ProductController;
