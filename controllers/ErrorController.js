const ErrorController = {};

ErrorController.UserIdError = class {
  constructor() {
    this.name = "UserIdError";
    this.message = "User doesn't exist";
    this.status_code = 404;
  }
};

ErrorController.UserEmailError = class {
  constructor() {
    this.name = "UserEmailError";
    this.message = "User email format is invalid";
    this.status_code = 400;
  }
};

ErrorController.UserDuplicateEmailError = class {
  constructor() {
    this.name = "UserDuplicateEmailError";
    this.message = "User email has already been registered";
    this.status_code = 409;
  }
};

ErrorController.UserDuplicateUsernameError = class {
  constructor() {
    this.name = "UserDuplicateUsernameError";
    this.message = "Username has already been used";
    this.status_code = 409;
  }
};

ErrorController.UserInfoError = class {
  constructor() {
    this.name = "UserInfoError";
    this.message = "User details must be provided";
    this.status_code = 400;
  }
};

ErrorController.UserTypeError = class {
  constructor() {
    this.name = "ProductTypeError";
    this.message = "User info don't match their respective data types";
    this.status_code = 400;
  }
};

ErrorController.UserUnregisteredError = class {
  constructor() {
    this.name = "UserUnregisteredError";
    this.message = "Username is not yet registered";
    this.status_code = 404;
  }
};

ErrorController.UserPasswordError = class {
  constructor() {
    this.name = "UserPasswordError";
    this.message = "User password is incorrect";
    this.status_code = 403;
  }
};

ErrorController.CartProductError = class {
  constructor() {
    this.name = "CartProductError";
    this.message = "Product ID and quantity must be provided";
    this.status_code = 400;
  }
};

ErrorController.CartAddError = class {
  constructor() {
    this.name = "CartAddError";
    this.message = "Product is already added in cart";
    this.status_code = 400;
  }
};

ErrorController.CartRemoveError = class {
  constructor() {
    this.name = "CartRemoveError";
    this.message = "Product ID must be provided";
    this.status_code = 400;
  }
};

ErrorController.ProductNotFound = class {
  constructor() {
    this.name = "ProductNotFound";
    this.message = "Product not found in user's cart";
    this.status_code = 404;
  }
};

ErrorController.ProductIdError = class {
  constructor() {
    this.name = "ProductIdError";
    this.message = "Product ID is invalid";
    this.status_code = 404;
  }
};

ErrorController.ProductTitleError = class {
  constructor() {
    this.name = "ProductTitleError";
    this.message = "Product title already exists";
    this.status_code = 409;
  }
};

ErrorController.ProductInactiveError = class {
  constructor() {
    this.name = "ProductInactiveError";
    this.message = "Product is currently not active";
    this.status_code = 404;
  }
};

ErrorController.ProductInfoError = class {
  constructor() {
    this.name = "ProductInfoError";
    this.message = "Complete product info must be provided";
    this.status_code = 400;
  }
};

ErrorController.ProductUpdateError = class {
  constructor() {
    this.name = "ProductUpdateError";
    this.message =
      "Either product name, description, or price should be provided";
    this.status_code = 400;
  }
};

ErrorController.ProductTypeError = class {
  constructor() {
    this.name = "ProductTypeError";
    this.message = "Product info don't match their respective data types";
    this.status_code = 400;
  }
};

ErrorController.OrderIdError = class {
  constructor() {
    this.name = "OrderIdError";
    this.message = "Order ID is invalid";
    this.status_code = 404;
  }
};

module.exports = ErrorController;
