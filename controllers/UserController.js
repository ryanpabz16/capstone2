const User = require("../models/User");
const Product = require("../models/Product");
const { hashSync, compareSync } = require("bcrypt");
const { createAccessToken } = require("../auth");
const ErrorController = require("./ErrorController");

const UserController = {};

UserController.getAllUsers = (request, response) => {
  return User.find({}, "-password -cart")
    .then((db_users) => {
      if (!db_users.length) {
        return response.status(200).json({ message: "No users registered" });
      }

      return response.status(200).json({ data: db_users });
    })
    .catch((error) =>
      response.status(500).json({ error: error.name, message: error.message })
    );
};

UserController.getUser = (request, response) => {
  return User.findById(request.params.username, "-password")
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();

      return response.status(200).json({ data: db_user });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.register = async (request, response) => {
  let { username, email, password } = request.body;

  await User.findById(username)
    .then((db_user) => {
      if (db_user) throw new ErrorController.UserDuplicateUsernameError();
    })
    .catch((error) => {
      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });

  return User.findOne({ email: email })
    .then((db_user) => {
      if (!username || !email || !password)
        throw new ErrorController.UserInfoError();
      if (db_user) throw new ErrorController.UserDuplicateEmailError();

      let new_user;
      try {
        new_user = new User({
          _id: username,
          email: email,
          password: hashSync(password, 10),
        });
      } catch (error) {
        throw new ErrorController.UserTypeError();
      }

      return new_user.save().then((registered_user) => {
        delete registered_user.password;

        return response.status(201).json({
          message: "New user has been registered successfully",
          data: registered_user,
        });
      });
    })
    .catch((error) => {
      if (error.name == "ValidationError")
        error = new ErrorController.UserEmailError();
      if (error.name == "CastError")
        error = new ErrorController.UserTypeError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.login = (request, response) => {
  let { username, password } = request.body;

  return User.findById(username)
    .then((db_user) => {
      if (!username || !password) throw new ErrorController.UserInfoError();
      if (!db_user) throw new ErrorController.UserUnregisteredError();

      if (!compareSync(password, db_user.password))
        throw new ErrorController.UserPasswordError();

      return response.status(201).json({
        message: "User has now logged in",
        token: createAccessToken(db_user),
        isAdmin: db_user.isAdmin,
      });
    })
    .catch((error) => {
      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.updateProfile = (request, response) => {
  const { fullName, birthDate, mobileNo, address } = request.body;
  return User.findByIdAndUpdate(
    request.params.username,
    {
      fullName: fullName,
      birthDate: birthDate,
      mobileNo: mobileNo,
      address: address,
    },
    { new: true }
  )
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();

      return response.status(200).json({
        message: "User profile successfully updated",
        data: db_user,
      });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.setUserAdmin = (request, response) => {
  return User.findByIdAndUpdate(
    request.params.username,
    { isAdmin: true },
    { new: true }
  )
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();

      return response.status(200).json({
        message: "User is currently set to admin",
        data: db_user,
      });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.getCartDetails = (request, response) => {
  return User.findById(request.params.username)
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();

      if (!db_user.cart.products.length) {
        return response
          .status(200)
          .json({ message: "User's cart is currently empty" });
      }

      return response.status(200).json({ data: db_user.cart });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.addToCart = (request, response) => {
  let { productId, quantity } = request.body;

  return User.findById(request.params.username)
    .then(async (db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();
      if (!productId || !quantity) throw new ErrorController.CartProductError();
      if (typeof quantity != "number")
        throw new ErrorController.ProductTypeError();
      if (
        db_user.cart.products.find((product) => product.productId == productId)
      ) {
        throw new ErrorController.CartAddError();
      }

      let price = await Product.findById(productId).then((db_product) => {
        if (!db_product) throw new ErrorController.ProductIdError();
        if (!db_product.isActive)
          throw new ErrorController.ProductInactiveError();

        return db_product.price;
      });

      let new_cart_order = {
        productId: productId,
        quantity: quantity,
        subTotal: price * quantity,
      };

      db_user.cart.products.push(new_cart_order);
      db_user.cart.totalAmount += new_cart_order.subTotal;

      db_user.save().then((updated_user) => {
        return response.status(201).json({
          message: "Products added to user cart successfully",
          data: updated_user.cart,
        });
      });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.updateCart = (request, response) => {
  let { productId, quantity } = request.body;

  return User.findById(request.params.username)
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();
      if (!productId || !quantity) throw new ErrorController.CartProductError();
      if (typeof quantity != "number")
        throw new ErrorController.ProductTypeError();

      if (!db_user.cart.products.length) {
        return response
          .status(200)
          .json({ message: "User's cart is currently empty" });
      }

      let db_cart_product = db_user.cart.products.find(
        (product) => product.productId == productId
      );
      if (!db_cart_product) throw new ErrorController.ProductNotFound();

      db_user.cart.totalAmount -= db_cart_product.subTotal;
      db_cart_product.subTotal =
        parseInt(db_cart_product.subTotal / db_cart_product.quantity) *
        quantity;
      db_cart_product.quantity = quantity;
      db_user.cart.totalAmount += db_cart_product.subTotal;

      db_user.save().then((updated_user) => {
        return response.status(200).json({
          message: "User's cart has been updated successfully",
          data: updated_user.cart,
        });
      });
    })
    .catch((error) => {
      if (error.name == "CastError")
        error = new ErrorController.ProductIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

UserController.removeFromCart = (request, response) => {
  let productId = request.body.productId;

  return User.findById(request.params.username)
    .then((db_user) => {
      if (!db_user) throw new ErrorController.UserIdError();
      if (!productId) throw new ErrorController.CartRemoveError();

      if (!db_user.cart.products.length) {
        return response
          .status(200)
          .json({ message: "User's cart is currently empty" });
      }

      let db_cart_product_index = db_user.cart.products.findIndex(
        (product) => product.productId == productId
      );
      if (db_cart_product_index < 0)
        throw new ErrorController.ProductNotFound();

      let db_cart_product = db_user.cart.products.splice(
        db_cart_product_index,
        1
      )[0];
      db_user.cart.totalAmount -= db_cart_product.subTotal;

      db_user.save().then((updated_user) => {
        return response.status(200).json({
          message: "User's cart has been updated successfully",
          data: updated_user.cart,
        });
      });
    })
    .catch((error) => {
      if (error.name == "CastError") error = new ErrorController.UserIdError();

      return response.status(error.status_code ?? 500).json({
        error: error.name,
        message: error.message,
      });
    });
};

module.exports = UserController;
