const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();
const PORT = process.env.PORT || 4000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@b303-pabualan.kz0zb91.mongodb.net/e-commerce-api?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
mongoose.connection.on("error", () =>
  console.log("Cannot connect to database!")
);
mongoose.connection.once("open", () => console.log("Connected to MongoDB!"));

app.use("/api/users", require("./routers/userRoutes"));
app.use("/api/products", require("./routers/productRoutes"));
app.use("/api/orders", require("./routers/orderRoutes"));

app.listen(PORT, () =>
  console.log(`E-commerce API is now online at localhost:${PORT}`)
);

module.exports = app;
