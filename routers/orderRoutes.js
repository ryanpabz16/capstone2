const router = require("express").Router();
const { verify, verifyAccess } = require("../auth");
const OrderController = require("../controllers/OrderController");

router.get("/all", verify, verifyAccess(), OrderController.getAllOrders);
router.get(
  "/:username",
  verify,
  verifyAccess(true, true),
  OrderController.getUserOrders
);
router.post(
  "/:username",
  verify,
  verifyAccess(false, true),
  OrderController.checkoutOrder
);

module.exports = router;
