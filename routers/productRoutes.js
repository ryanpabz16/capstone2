const router = require("express").Router();
const { verify, verifyAccess } = require("../auth");
const ProductController = require("../controllers/ProductController");

router.get("/all", ProductController.getAllProducts);
router.get("/", ProductController.getActiveProducts);
router.get("/:productId", ProductController.getProduct);
router.post("/", verify, verifyAccess(), ProductController.createProduct);
router.put(
  "/:productId",
  verify,
  verifyAccess(),
  ProductController.updateProduct
);
router.put(
  "/:productId/archive",
  verify,
  verifyAccess(),
  ProductController.archiveProduct
);
router.put(
  "/:productId/activate",
  verify,
  verifyAccess(),
  ProductController.activateProduct
);

module.exports = router;
