const router = require("express").Router();
const { verify, verifyAccess } = require("../auth");
const UserController = require("../controllers/UserController");

router.get("/all", verify, verifyAccess(), UserController.getAllUsers);
router.get(
  "/:username",
  verify,
  verifyAccess(true, true),
  UserController.getUser
);
router.put(
  "/:username",
  verify,
  verifyAccess(false, true),
  UserController.updateProfile
);
router.post("/register", UserController.register);
router.post("/login", UserController.login);
router.put(
  "/:username/admin",
  verify,
  verifyAccess(),
  UserController.setUserAdmin
);
router.get(
  "/:username/cart",
  verify,
  verifyAccess(true, true),
  UserController.getCartDetails
);
router.post(
  "/:username/cart",
  verify,
  verifyAccess(false, true, true),
  UserController.addToCart
);
router.put(
  "/:username/cart/update",
  verify,
  verifyAccess(false, true, true),
  UserController.updateCart
);
router.put(
  "/:username/cart/remove",
  verify,
  verifyAccess(false, true, true),
  UserController.removeFromCart
);

module.exports = router;
