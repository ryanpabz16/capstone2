const { Schema, model } = require("mongoose");
const { isEmail } = require("validator");

const user_schema = new Schema({
  _id: {
    type: String,
    required: true,
    cast: false,
  },
  email: {
    type: String,
    validate: isEmail,
    required: true,
    cast: false,
  },
  password: {
    type: String,
    required: true,
    cast: false,
  },
  fullName: {
    type: String,
    default: "",
  },
  birthDate: {
    type: String,
    default: "",
  },
  mobileNo: {
    type: String,
    default: "",
  },
  address: {
    type: String,
    default: "",
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  cart: {
    products: [
      {
        productId: {
          type: String,
          required: true,
          cast: false,
        },
        quantity: {
          type: Number,
          required: true,
          cast: false,
        },
        subTotal: {
          type: Number,
          default: 0,
        },
      },
    ],
    totalAmount: {
      type: Number,
      default: 0,
    },
  },
});

module.exports = model("User", user_schema);
