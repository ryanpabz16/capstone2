const { Schema, model } = require("mongoose");

const product_schema = new Schema({
  title: {
    type: String,
    required: true,
    cast: false,
  },
  description: {
    type: String,
    required: true,
    cast: false,
  },
  price: {
    type: Number,
    required: true,
    cast: false,
  },
  platforms: [
    {
      type: String,
    },
  ],
  genre: [
    {
      type: String,
    },
  ],
  metascore: {
    type: Number,
    required: true,
    cast: false,
  },
  releaseDate: {
    type: String,
    required: true,
    cast: false,
  },
  publisher: {
    type: String,
    required: true,
    cast: false,
  },
  developer: {
    type: String,
    required: true,
    cast: false,
  },
  ageRating: {
    type: String,
    required: true,
    cast: false,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  addedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = model("Product", product_schema);
