const { Schema, model } = require("mongoose");

const order_schema = new Schema({
  username: {
    type: String,
    required: [true, "User ID is required"],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Product quantity is required"],
      },
      subTotal: {
        type: Number,
        default: 0,
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 0,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = model("Order", order_schema);
