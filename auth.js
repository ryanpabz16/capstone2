const jwt = require("jsonwebtoken");
const KEY = process.env.JWT_KEY;

const auth = {};

auth.createAccessToken = (user) => {
  const user_data = {
    _id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(user_data, KEY, {});
};

auth.verify = (request, response, next) => {
  let token = request.headers.authorization;

  if (!token) {
    return response.status(401).json({
      message: "Authentication failed! User token must be provided",
    });
  }
  token = token.split(" ")[1];

  return jwt.verify(token, KEY, (error, decoded_user) => {
    if (error) {
      return response.status(401).json({
        message: "Authentication failed! User token is invalid",
        error: error,
      });
    }

    request.user = decoded_user;
    return next();
  });
};

auth.verifyAccess = (
  adminAccess = true,
  userAccess = false,
  strictlyNoAdmin = false
) => {
  return (request, response, next) => {
    let isAdmin = request.user.isAdmin;
    let isUser = request.params.username == request.user._id;

    if (strictlyNoAdmin && isAdmin) {
      return response
        .status(403)
        .json({ message: "Action is forbidden to admin users" });
    } else if (!((adminAccess && isAdmin) || (userAccess && isUser))) {
      return response.status(403).json({ message: "Action is forbidden" });
    }

    return next();
  };
};

module.exports = auth;
